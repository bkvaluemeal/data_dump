from setuptools import setup, find_packages

def format(input, start = 0):
	result = ''
	indent = False
	count = 0

	with open(input, 'r') as file:
		for line in file:
			if count > start:
				if line[:1] == '\t' and not indent:
					indent = True
					result += '::\n\n'

				if line[:1].isalnum() and indent:
					indent = False

				result += line.replace('> ', '\t').replace('>', '\t')
			count += 1

	return result

blurb = ('Data Dump is a Python library that is designed to collect mass'
	'ammounts of sample data for training artificial intelligence models.\n'
)

setup(
	name = 'data_dump',
	version = '0.1.0',
	author = 'Justin Willis',
	author_email = 'sirJustin.Willis@gmail.com',
	packages = find_packages(),
	include_package_data = True,
	zip_safe = False,
	url = 'https://bitbucket.org/bkvaluemeal/data_dump',
	license = 'ISC License',
	description = 'A Python library designed to collect mass amounts of data',
	long_description = blurb + format('README.md', 3),
	classifiers = [
		'Development Status :: 3 - Alpha',
		'Environment :: Console',
		'Intended Audience :: Science/Research',
		'License :: OSI Approved :: ISC License (ISCL)',
		'Operating System :: OS Independent',
		'Programming Language :: Python :: 3',
		'Topic :: Scientific/Engineering :: Artificial Intelligence'
	],
	keywords = 'collect sample training data artificial intelligence',
	install_requires = [
	]
)
